# Readme

code goes to "src" -folder

[Start up guide for whole Cayac](https://gitlab.labranet.jamk.fi/wimma-lab-2019/mysticons/cayac/start-up/blob/master/README.md)

If you want to use this configuration on your own project follow these steps. 

You need google cloud account that has credits you can use different cloud service but you have to configure .gitlab-ci.yml file to suite that.
Understanding of docker is also advised as you need to build your own image from the source code.
Also gitlab ultimate is required for SAST. 

You also need to install gitlab runner, kubectl and Docker on your machine.

1.  Copy the [source](https://gitlab.labranet.jamk.fi/wimma-lab-2019/mysticons/cayac/source) repository under Cayac group 
2.  register a gitlab runner so that it can run our jobs in CI/CD pipeline. registeration token and URL you will find in settings -> CI/CD -> runners
```
sudo gitlab-runner register -n \
   --url "CHANGE_THIS" \
   --registration-token "CHANGE_THIS" \
   --executor docker \
   --description "Runner-for-gitlab-tools" \
   --docker-image "docker:stable" \
   --docker-privileged
```
3.  make 2 new kubernetes clusters 
4.  retrieve service key from google cloud. Good instructions [here](https://gitlab.labranet.jamk.fi/wimma-lab-2019/mysticons/cayac/start-up/blob/master/README.md)
5.  add it to the gitlab variables as SERVICE_ACCOUNT_KEY
6.  change cluster names and other information in gitlab-ci.yml 
7.  change the dockerfile so it will build your project into image
8.  change image in the deployment files
9.  create 2 static ip addres for clusters
```
gcloud compute addresses create demo-dev-ip --region europe-north1
gcloud compute addresses describe demo-dev-ip --region europe-north1
```
10. change these addresses to deployment files in LoadBalancerIP
11. use the ip in deployment file for testing and the IP deployment_production for accesing your production service.
12. remember to change the ports in dockerfile and deployment files